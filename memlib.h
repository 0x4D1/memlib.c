#pragma once

#include <Windows.h>

// Memlib.c - Memory modification library by 4D 1
// Nothing special here, just wanted a simple generic memory library


namespace memlib {
	
	// Pattern Scanner
	class PatternScanner {
	public:

		// Initializer to determine scan range.
		PatternScanner(DWORD _start, DWORD _size);
		PatternScanner(char* moduleName = NULL);
		PatternScanner(HMODULE _module);

		// Actual pattern finder.
		DWORD FindPattern(char* pattern, char* mask, DWORD offset);

	private:
		DWORD base_;
		DWORD size_;
	};
	
	
	
	// Hooker (x86 JMP hook)
	class Hook {
	public:
		BYTE* Detour(BYTE* _source, BYTE* _detour);
		void Retour();
		
	private:
		BYTE* retour_func_;
		BYTE* source_;
		DWORD length_;
	};
	
	
	
	
	// Module shit
	bool UnlinkModule(HMODULE module);
}