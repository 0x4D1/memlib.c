#include "..\memlib.h"

extern "C" {
#include "ld32\ld32.h"
}


DWORD CalculateDetourLength(BYTE* _source) {
	
	DWORD len = 0;
	DWORD current_op;

	do{
		current_op = length_disasm((void*)_source);
		if (current_op != 0){
			len += current_op;
			_source += current_op;
		}
	} while (len < 5);

	return len;
}

void memlib::Hook::Retour() {
	DWORD old_protection;

	VirtualProtect(source_, length_, PAGE_READWRITE, &old_protection);

	memcpy(source_, retour_func_, length_);

	VirtualProtect(source_, length_, old_protection, &old_protection);

	delete[] retour_func_;
}

BYTE* memlib::Hook::Detour(BYTE* _source, BYTE* _detour) {
	DWORD old_protection;

	source_ = _source;
	length_ = CalculateDetourLength(_source);
	retour_func_ = (BYTE*)malloc(length_ + 5);
	VirtualProtect(retour_func_, length_ + 5, PAGE_EXECUTE_READWRITE, &old_protection);

	memcpy(retour_func_, source_, length_);

	retour_func_ += length_;
	
	retour_func_[0] = 0xE9;
	*(DWORD*)(retour_func_ + 1) = (DWORD)((source_ + length_) - (retour_func_ + 5));

	VirtualProtect(source_, length_, PAGE_EXECUTE_READWRITE, &old_protection);

	source_[0] = 0xE9;
	*(DWORD*)(source_ + 1) = (DWORD)(_detour - (source_ + 5));

	if (length_ != 5)
		for (DWORD i = 5; i < length_;i++)
			source_[i] = 0x90;

	VirtualProtect(source_, length_, old_protection, &old_protection);

	retour_func_ -= length_;

	return retour_func_;
}

