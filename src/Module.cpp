#include "..\memlib.h"

typedef struct tUNICODE_STRING
{
	USHORT Length;
	USHORT MaximumLength;
	PWSTR Buffer;
}UNICODE_STRING;

typedef struct tModuleInfoNode
{
	LIST_ENTRY LoadOrder;
	LIST_ENTRY InitOrder;
	LIST_ENTRY MemoryOrder;
	HMODULE BaseAddress;
	unsigned long EntryPoint;
	unsigned int Size;
	UNICODE_STRING FullPath;
	UNICODE_STRING Name;
	unsigned long Flags;
	unsigned short LoadCount;
	unsigned short TlsIndex;
	LIST_ENTRY HashTable;
	unsigned long TimeStamp;
}ModuleInfoNode;

typedef struct tProcessModuleInfo
{
	unsigned int Size;
	unsigned int Initialized;
	HANDLE SsHandle;
	LIST_ENTRY LoadOrder;
	LIST_ENTRY InitOrder;
	LIST_ENTRY MemoryOrder;
}ProcessModuleInfo;


bool memlib::UnlinkModule(HMODULE module)
{
	BYTE* peb = (BYTE*)__readfsdword(0x30);
	ProcessModuleInfo* pInfo = *(ProcessModuleInfo**)(peb + 0xC);
	ModuleInfoNode* pNode = (ModuleInfoNode*)(pInfo->LoadOrder.Flink);
	
	while(pNode && pNode->BaseAddress != module)
		pNode = (ModuleInfoNode*)(pNode->LoadOrder.Flink);
	
	if(!pNode) return false;
	
	
	LIST_ENTRY* pLoad;
	
	pLoad = &pNode->LoadOrder;
	
	pLoad->Blink->Flink = pLoad->Flink; 
	pLoad->Flink->Blink = pLoad->Blink;
	
	pLoad = &pNode->InitOrder;
	
	pLoad->Blink->Flink = pLoad->Flink; 
	pLoad->Flink->Blink = pLoad->Blink;
	
	pLoad = &pNode->MemoryOrder;
	
	pLoad->Blink->Flink = pLoad->Flink; 
	pLoad->Flink->Blink = pLoad->Blink;
	
	pLoad = &pNode->HashTable;
	
	pLoad->Blink->Flink = pLoad->Flink; 
	pLoad->Flink->Blink = pLoad->Blink;
	
	return true;
}